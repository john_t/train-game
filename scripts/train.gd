extends PathFollow2D
class_name Train

export (float) var speed = 128
export (Color) var color = Color(0, 0, 0) setget set_color
var mul: float = 1
var can_switch: bool = true
onready var game = get_node("/root/Game/train_nodes")

func _process(delta):
	self.offset += speed * delta * mul
	$sprite.flip_v = mul < 0
	if can_switch:
		var track = get_node("../..")
		var node: Node
		var start_distance = self.position.distance_to(track.start)
		var end_distance = self.position.distance_to(track.end)
		if start_distance < end_distance:
			node = track.start_node
		else:
			node = track.end_node
		var distance = min(start_distance, end_distance)
		if distance <= 2 * speed * delta:
			can_switch = false
			var has_switched = false
			for line in game.get_node("lines").get_children():
				if line != track and \
					line.color == self.color and \
					(line.start_node == node or line.end_node == node):
					has_switched = true
					self.get_parent().remove_child(self)
					line.get_node("path").add_child(self)
					if line.start_node == node:
						self.offset = 0
						self.mul = abs(self.mul)
					else:
						self.unit_offset = 1
						self.mul = -abs(self.mul)
					break
			if !has_switched:
				self.mul *= -1
	else:
		if (self.unit_offset > 0.5 and self.mul > 0) or \
			(self.unit_offset < 0.5 and self.mul < 0):
			can_switch = true

func set_color(val: Color):
	color = val
	$sprite.self_modulate = val
