tool
extends Node2D

export (Vector2) var start = Vector2(0, 0) setget set_start
export (NodePath) var start_node = null
export (Vector2) var end = Vector2(0, 0) setget set_end
export (NodePath) var end_node = null
export (PackedScene) var train
export (Color) var color setget set_color
export (float) var default_width = 10
export (float) var scale_width = 15
export (float) var tween_time = 0.3
export (bool) var mouse_in = false

func _ready():
	$area.input_pickable = true
	print("connection: " + str($area.connect("mouse_entered", self, "_on_area_mouse_entered")))
	$area.connect("mouse_exited", self, "_on_area_mouse_exited")
	$area.connect("input_event", self, "_on_area_input_event")
	$path.curve = $path.curve.duplicate(true)

func set_start(val: Vector2) ->	void:
	start = val
	self._recalculate_points()

func set_end(val: Vector2) -> void:
	end = val
	self._recalculate_points()

func set_color(val: Color) -> void:
	get_node("line").default_color = val
	color = val

func _recalculate_points() -> void:
	var line = $line
	if line != null:
		line.set_point_position(0, start);
		var x_dist = start.x - end.x;
		var y_dist = start.y - end.y;
		var midpoint = Vector2(0, 0)
		if abs(x_dist) > abs(y_dist):
			midpoint = Vector2(
				end.x + (sign(x_dist) * abs(y_dist)),
				start.y
			)
		else:
			midpoint = Vector2(
				start.x,
				end.y + (sign(y_dist) * abs(x_dist))
			)
		line.set_point_position(1, midpoint);
		line.set_point_position(2, end);
		line.gen_coll()
		_sync_line_to_path()


func _on_area_mouse_entered():
	self.mouse_in = true
	$tween.interpolate_property(
		$line,
		"width",
		$line.width,
		scale_width,
		tween_time,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN_OUT
	)
	$tween.start()

func _on_area_mouse_exited():
	self.mouse_in = false
	$tween.interpolate_property(
		$line,
		"width",
		$line.width,
		default_width,
		tween_time,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN_OUT
	)
	$tween.start()

func _on_area_input_event(viewport, event, idx):
	pass

func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_RIGHT and event.pressed and mouse_in:
			self.get_parent().get_parent().delete_track(self)
		if event.button_index == BUTTON_MIDDLE and event.pressed and mouse_in:
			self.create_train()

func _sync_line_to_path():
	get_node("path").curve.clear_points()
	for point in $line.points:
		get_node("path").curve.add_point(point)

func create_train() -> void:
	var train = self.train.instance()
	train.color = self.color
	train.position = Vector2.ZERO
	get_node("./path").add_child(train)
