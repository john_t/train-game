extends Node2D

enum PassengerType {
	SQUARE,
	TRIANGLE,
	CIRCLE,
}

export (PackedScene) var passenger_square
export (PackedScene) var passenger_triangle
export (PackedScene) var passenger_circle
export (Vector2) var default_scale = Vector2(1, 1)
export (Vector2) var expanded = Vector2(0.3, 0.3)
export (float) var expand_radius = 48.0
export (float) var radius = 24.0
var passengers = [];

func _ready():
	self.scale = Vector2(
		default_scale.x + expanded.x,
		default_scale.y + expanded.y
	)

func _input(event):
	if event is InputEventMouseMotion:
		var position = self.get_global_mouse_position()
		var distance = self.position.distance_to(position);
		var interpolate = max(min(1 - pow( (distance / expand_radius), 3 ), 1), 0)
		var scale = Vector2(
			default_scale.x + (expanded.x * interpolate),
			default_scale.y + (expanded.y * interpolate)
		)
		self.scale = scale
		get_node("passengers").scale = Vector2(1 / scale.x, 1 / scale.y)
	elif event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed and is_clicked():
			self.get_parent().get_parent().begin_connection(self)
		if event.button_index == BUTTON_RIGHT and event.pressed and is_clicked():
			self.get_parent().get_parent().delete_node(self)
		if event.button_index == BUTTON_MIDDLE and event.pressed and is_clicked():
			self.passengers.append(randi() % 3)
			draw_passengers()

func is_clicked():
	var position = self.get_global_mouse_position()
	var distance = self.position.distance_to(position);
	return distance <= expand_radius

func draw_passengers():
	for child in $passengers.get_children():
		child.queue_free()
	var i = 0
	for passen in self.passengers:
		var current = null
		if passen == PassengerType.SQUARE:
			current = passenger_square.instance()
		elif passen == PassengerType.CIRCLE:
			current = passenger_circle.instance()
		elif passen == PassengerType.TRIANGLE:
			current = passenger_triangle.instance()
		$passengers.add_child(current)
		current.position = Vector2(i * 32, 0)
		i += 1
