extends TextureButton

class_name ScaleToggleButton, "res://images/passengers/triangle.svg"
export (float) var scale_factor = 1.25;
export (float) var toggle_scale_factor = 0.5;
export (float) var transition_speed = 4
export (NodePath) var manager = ".."
var target_scale = 1.0
var toggle_call = true

func _ready():
	self.connect("toggled", self, "_on_Toggle")
	self.connect("mouse_entered", self, "_on_Mouse_Entered")
	self.connect("mouse_exited", self, "_on_Mouse_Exited")
	_compute_size()

func _process(delta):
	self.rect_pivot_offset = self.rect_size / Vector2(2, 2)
	var scale = self.rect_scale.x;
	var diff = scale - target_scale
	if abs(diff) > transition_speed * delta:
		self.rect_scale += Vector2(transition_speed, transition_speed) * delta * -sign(diff)

func _on_Mouse_Entered():
	get_node(get_node(manager).game_node).allow_input = false
	self.target_scale = scale_factor

func _on_Mouse_Exited():
	get_node(get_node(manager).game_node).allow_input = true
	_compute_size()

func _on_Toggle(toggled: bool):
	_compute_size()
	if toggle_call:
		self.pressed = true
		get_node(manager).emit_signal("changed", self.name)

func _compute_size():
	if self.pressed:
		self.target_scale = toggle_scale_factor
	else:
		self.target_scale = 1

func disable():
	self.toggle_call = false
	self.target_scale = 1
	self.pressed = false
	self.toggle_call = true
