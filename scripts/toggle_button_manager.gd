extends Node

class_name ToggleButtonManager, "res://images/nodes/triangle.svg"
signal changed(val)

export (NodePath) var game_node = "/root/Game/train_nodes"

func _ready():
	self.connect("changed", self, "_disable")
	self.connect("mouse_entered", self, "_on_Mouse_Entered")
	self.connect("mouse_exited", self, "_on_Mouse_Exited")

func _disable(val):
	for child in self.get_children():
		if child is ScaleToggleButton and child.name != val:
			child.disable()

func _on_Mouse_Entered():
	get_node(game_node).allow_input = false

func _on_Mouse_Exited():
	get_node(game_node).allow_input = true
