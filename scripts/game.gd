extends Node2D

export (PackedScene) var node
export (PackedScene) var train
export (String) var node_prefix = "res://scenes/instance/nodes/"
export (PackedScene) var track
export (Color) var color
export (bool) var allow_input = true
var connections = []
var current = null
var current_strand = null;
var move = null

func _read():
	randomize()

func _input(event):
	if allow_input:
		if event is InputEventMouseButton:
			if event.is_action_pressed("click"):
				self.create_new_node();
			if event.button_index == BUTTON_LEFT and !event.pressed:
				self.finish_line()
		elif event is InputEventMouseMotion:
			self.drag_line()

func begin_connection(node: Node2D):
	current = node
	current_strand = track.instance();
	$lines.add_child(current_strand)
	current_strand.train = self.train
	current_strand.start = current.position
	current_strand.start_node = current
	current_strand.end = current.position
	current_strand.color = color

func create_new_node():
	if not nodes_in_proximity() and node != null:
		var instance = node.instance()
		$stations.add_child(instance)
		instance.position = self.get_global_mouse_position()

func nodes_in_proximity() -> bool:
	var applicable = true
	for child in $stations.get_children():
		var position = self.get_global_mouse_position()
		var distance = child.position.distance_to(position);
		applicable = applicable and distance >= child.radius * 2;
	return not applicable

func drag_line():
	if current != null and current_strand != null:
		current_strand.end = self.get_global_mouse_position();

func finish_line():
	if current_strand != null:
		var complete = false
		var position = self.get_global_mouse_position()
		var node = self.get_node_at_pos(position)
		if node != null:
			current_strand.end = node.position
			current_strand.end_node = node
			complete = true

		if complete:
			for line in $lines.get_children():
				if ((
					line.start_node == current and \
					line.end_node == node \
				) or (
					line.start_node == current and \
					line.end_node == node \
				)) and line != current_strand:
					complete = false
		if !complete:
			$lines.remove_child(current_strand)
		current_strand = null
		current = null

func delete_node(node: Node2D):
	for child in $lines.get_children():
		if \
			child.start_node == node or \
			child.end_node == node:
			child.queue_free()
	node.queue_free()

func delete_track(node: Node2D):
	node.queue_free()

func _on_shape_container_changed(val):
	self.node = load(node_prefix + str(val) + ".tscn")

func _on_color_containers_changed(val):
	var node: Node = get_node("../CanvasLayer/Control/PanelContainer/box/color_containers/" + val)
	var color: Color = node.self_modulate
	self.color = color

func get_node_at_pos(pos: Vector2) -> Node:
	var position = get_global_mouse_position()
	for child in $stations.get_children():
		var distance = child.position.distance_to(position);
		if distance <= child.expand_radius:
			return child
	return null
