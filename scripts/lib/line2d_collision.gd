extends Line2D

func gen_coll():
	for child in get_node("../area").get_children():
		child.queue_free()
	for i in points.size() - 1:
		var new_shape = CollisionShape2D.new()
		get_node("../area").add_child(new_shape)
		var rect = RectangleShape2D.new()
		new_shape.position = (points[i] + points[i + 1]) / 2
		new_shape.rotation = points[i].direction_to(points[i + 1]).angle()
		var length = points[i].distance_to(points[i + 1])
		rect.extents = Vector2(length / 2, self.width / 2.0)
		new_shape.shape = rect
